package com.example.posttestsoundpoolandmediaplayer

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build.VERSION
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_sound_pool.*

@Suppress("DEPRECATION")
class SoundPoolActivity : AppCompatActivity(), View.OnClickListener {

    private val MAX_STREAMS = 1
    private var loaded = false
    private var soundId = 0
    private lateinit var soundPool: SoundPool
    private lateinit var btnPlaySoundPool: Button
    private lateinit var buttonBackSoundPool: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sound_pool)

        buttonBackSoundPool = findViewById(R.id.btnSoundPoolBack)
        buttonBackSoundPool.setOnClickListener(this)

        btnPlaySoundPool = findViewById(R.id.btnSoundPoolPlay)
        btnPlaySoundPool.setOnClickListener(this)

        var playSound: Int = 0

        if (VERSION.SDK_INT >= 21) {
            val audioAttrib = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
            val builder = SoundPool.Builder()
            builder.setAudioAttributes(audioAttrib).setMaxStreams(MAX_STREAMS)
            this.soundPool = builder.build()

        } else this.soundPool = SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0)

        this.soundPool.setOnLoadCompleteListener { soundPool, i, i2 -> loaded = true

        }

        this.soundId = this.soundPool.load(this, R.raw.sound, 1)

        val mgr = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val volume = actualVolume / maxVolume

        if (loaded) {
            this.soundPool.play(this.soundId, volume, volume, 1, 0, 1f)
        } else {
            Toast.makeText(this, "Sound Pool Not Loaded", Toast.LENGTH_LONG).show()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id) {

            R.id.btnSoundPoolPlay -> {
                soundPool.play(soundId, 1f, 1f, 0, 0, 1f)
                soundPool.autoPause()
            }

            R.id.btnSoundPoolBack -> {
                onBackPressed()
            }
        }
    }
}