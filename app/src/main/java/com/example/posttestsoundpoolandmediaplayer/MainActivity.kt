package com.example.posttestsoundpoolandmediaplayer

import android.content.Intent
import android.media.SoundPool
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var buttonSound: Button
    private lateinit var buttonVideo: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Button for Intent to Sound Pool
        buttonSound = findViewById(R.id.btnSound)
        buttonSound.setOnClickListener (this)

        //Button for Intent to Video
        buttonVideo = findViewById(R.id.btnVideo)
        buttonVideo.setOnClickListener (this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSound -> {
                val intent = Intent(this, SoundPoolActivity::class.java)
                startActivity(intent)
            }
            R.id.btnVideo -> {
                val intent = Intent(this, VideoActivity::class.java)
                startActivity(intent)
            }
        }
    }
}