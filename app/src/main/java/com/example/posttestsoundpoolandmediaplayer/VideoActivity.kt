package com.example.posttestsoundpoolandmediaplayer

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContentProviderCompat.requireContext
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val path = "android.resource://$packageName/${R.raw.video}"
        vvPlayer.setVideoURI(Uri.parse(path))

        btnVideoPlay.setOnClickListener {
            vvPlayer.start()
        }
        btnVideoPause.setOnClickListener {
            vvPlayer.pause()
        }
        btnVideoBack.setOnClickListener {
            onBackPressed()
        }
    }
}